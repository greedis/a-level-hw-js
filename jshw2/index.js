// TASK1
let array = ['Капуста', 'Репа', 'Редиска', 'Морковка'];
console.log(array.join('|'));
// TASK2
let string = 'Вася;Петя;Вова;Олег';
const newArr = string.split([';']);

// TASK3
function hello2(name = 'гость') {
    console.log('Привет, ' + name);
}

hello2('Илья');
// TASK4
const fruits = ['яблоко', 'ананас', 'груша'];
const fruitsInUpperCase = fruits.map(function (x) {
    return x.toUpperCase()
});

// TASK5
function addOneForAll(...args) {
    // args = Number(args);
    let arr = args;
    return arr.map(function (x) {
        return x + 1
    });
}

const val = addOneForAll(1, 2, 3, 4);
console.log(val);

// TASK6
function getSum(...args) {
    let arr = args;
    let n = 0;
    for (i = 0; i < arr.length; i++) {
        n += arr[i];
    }
    return n;
}
const val2 = getSum(1, 2, 3, 4);
console.log(val2); // 10
// TASK7
const arr = [1, 'hello', 2, 3, 4, '5', '6', 7, null];
const numberArray = arr.filter(el => typeof el == 'number'); // [1, 2, 3, 4, 7];
// TASK8
function arrayTesting(arr) {
    if (arr.some(el => el == true)) {
        return 'Нашли true значение';
    } else {
        return 'Ничего нет';
    }
}
const haveTrueValue = arrayTesting([0, false, null, 1]);
const dontHaveTrueValue = arrayTesting([0, false, null, 0]);